/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.pa.DAOs;

import co.edu.uniminuto.pa.DTOs.Persona;
import co.edu.uniminuto.pa.DTOs.Producto;
import co.edu.uniminuto.pa.DTOs.Tarjeta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nixoduaa
 */
public class TarjetaDAO{
    
    public ArrayList<Tarjeta> crearSolicitudTarjeta(Tarjeta p, Connection con){
        ArrayList<Tarjeta> datos = new ArrayList();
        Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select nombre1, apellido1, cedula from cliente ");
            while (rs.next())
            { 
                Tarjeta per = new Tarjeta();
                per.setNombre1(rs.getString(1));
                per.setApellido1(rs.getString(2));
                per.setIdentificacion(rs.getString(3));
                datos.add(per);
            }
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public ArrayList<Tarjeta> consultarPersona(Tarjeta p, Connection con){
        ArrayList<Tarjeta> datos = new ArrayList();
        Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select nombre1, apellido1, cedula from cliente "
                    + "where nombre1='" + p.getNombre1()+"' AND "
                    + "apellido1='" + p.getApellido1()+"'");
            while (rs.next())
            { 
                Tarjeta per = new Tarjeta();
                per.setNombre1(rs.getString(1));
                per.setApellido1(rs.getString(2));
                per.setIdentificacion(rs.getString(3));
                datos.add(per);
            }
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public boolean crearSolicitud(Tarjeta p,Producto pr,Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        int numero = (int) (Math.random() * 2) + 1;
        String num = Integer.toString(numero);
        String estado = null;
        try {            
            if(numero==1)
                estado="aprobada";
            else if(numero==2)
                estado="rechazada";
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando crearPersona...");
            pstmt = con.prepareStatement("INSERT INTO solicitud "
                    + " VALUES (?,?,?,?,?)");
            pstmt.setString(1, num);
            pstmt.setString(2, estado);
            pstmt.setString(3, "15/11/2017");
            pstmt.setString(4, p.getIdentificacion());
            pstmt.setString(5, pr.getId());
            pstmt.execute();            
            con.close();            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return respuesta;
    }
    
    public ArrayList<Tarjeta> tarjetaConsultar(Tarjeta p, Connection con){
        ArrayList<Tarjeta> datos = new ArrayList();
        Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select cedula,nombre1,nombre2,apellido1,apellido2,"
            + "id_producto,nombre,id_solicitud\n" +
            ",estado,cliente_cedula,producto_id_producto \n" +
            "from cliente cl,producto pr,solicitud sl \n" +
            "where cl.cedula = sl.cliente_cedula AND\n" +
            "pr.id_producto = sl.producto_id_producto AND "
            + "nombre1='"+p.getNombre1()+"' AND "
            + "apellido1='"+p.getApellido1()+"' ");
            while (rs.next())
            { 
                Tarjeta rep = new Tarjeta();
                rep.setIdentificacion(rs.getString(1));
                rep.setNombre1(rs.getString(2));
                rep.setNombre2(rs.getString(3));
                rep.setApellido1(rs.getString(4));
                rep.setApellido2(rs.getString(5));
                rep.setId_producto(rs.getString(6));
                rep.setNombre(rs.getString(7));
                rep.setId_solicitud(rs.getString(8));
                rep.setEstado(rs.getString(9));
                rep.setCliente_cedula(rs.getString(10));
                rep.setProducto_id_producto(rs.getString(11));
                datos.add(rep);
                
            }
            
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(TarjetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public boolean actualizarSolicitud(Tarjeta p,Producto pr,Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        int numero = (int) (Math.random() * 2) + 1;
        String num = Integer.toString(numero);
        String estado = null;
        try {            
            if(numero==1)
                estado="aprobada";
            else if(numero==2)
                estado="rechazada";
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando crearPersona...");
            pstmt = con.prepareStatement("UPDATE solicitud "
                    + " SET id_solicitud=?,estado=?,fecha_solicitud=?,cliente_cedula=?,"
                    + "producto_id_producto=?");
            pstmt.setString(1, num);
            pstmt.setString(2, estado);
            pstmt.setString(3, "15/11/2017");
            pstmt.setString(4, p.getIdentificacion());
            pstmt.setString(5, pr.getId());
            pstmt.execute();            
            con.close();            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return respuesta;
    }
}
