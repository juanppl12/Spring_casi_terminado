package co.edu.uniminuto.pa.DAOs;

import co.edu.uniminuto.pa.DTOs.Producto;
import co.edu.uniminuto.pa.DTOs.Tarjeta;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductoDAO {
        public ArrayList<Producto> crearSolicitudProducto(Producto p, Connection con){
        ArrayList<Producto> datos = new ArrayList();
        Logger.getLogger(ProductoDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Producto...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select nombre,id_producto from producto");
            while (rs.next())
            { 
                Producto per = new Producto();
                per.setNombre(rs.getString(1));
                per.setId(rs.getString(2));
                datos.add(per);
            }
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando producto fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
}
