/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.pa.DAOs;

import co.edu.uniminuto.pa.DTOs.Persona;
import co.edu.uniminuto.pa.DTOs.Reportes;
import co.edu.uniminuto.pa.DTOs.Tarjeta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nixoduaa
 */
public class ReportesDAO{
    
    public ArrayList<Reportes> solicitudAprobada(Reportes p, Connection con){
        ArrayList<Reportes> datos = new ArrayList();
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select cedula,nombre1,nombre2,apellido1,apellido2,"
            + "id_producto,nombre,id_solicitud\n" +
            ",estado,cliente_cedula,producto_id_producto \n" +
            "from cliente cl,producto pr,solicitud sl \n" +
            "where cl.cedula = sl.cliente_cedula AND\n" +
            "pr.id_producto = sl.producto_id_producto AND\n" +
            "sl.estado = 'aprobada'");
            while (rs.next())
            { 
                Reportes rep = new Reportes();
                rep.setCedula(rs.getString(1));
                rep.setNombre1(rs.getString(2));
                rep.setNombre2(rs.getString(3));
                rep.setApellido1(rs.getString(4));
                rep.setApellido2(rs.getString(5));
                rep.setId_producto(rs.getString(6));
                rep.setNombre(rs.getString(7));
                rep.setId_solicitud(rs.getString(8));
                rep.setEstado(rs.getString(9));
                rep.setCliente_cedula(rs.getString(10));
                rep.setProducto_id_producto(rs.getString(11));
                datos.add(rep);
                
            }
            
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public ArrayList<Reportes> solicitudRechazada(Reportes p, Connection con){
        ArrayList<Reportes> datos = new ArrayList();
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select cedula,nombre1,nombre2,apellido1,apellido2,"
            + "id_producto,nombre,id_solicitud\n" +
            ",estado,cliente_cedula,producto_id_producto \n" +
            "from cliente cl,producto pr,solicitud sl \n" +
            "where cl.cedula = sl.cliente_cedula AND\n" +
            "pr.id_producto = sl.producto_id_producto AND\n" +
            "sl.estado = 'rechazada'");
            while (rs.next())
            { 
                Reportes rep = new Reportes();
                rep.setCedula(rs.getString(1));
                rep.setNombre1(rs.getString(2));
                rep.setNombre2(rs.getString(3));
                rep.setApellido1(rs.getString(4));
                rep.setApellido2(rs.getString(5));
                rep.setId_producto(rs.getString(6));
                rep.setNombre(rs.getString(7));
                rep.setId_solicitud(rs.getString(8));
                rep.setEstado(rs.getString(9));
                rep.setCliente_cedula(rs.getString(10));
                rep.setProducto_id_producto(rs.getString(11));
                datos.add(rep);
                
            }
            
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    public ArrayList<Reportes> solicitudPersona(Reportes p, Connection con){
        ArrayList<Reportes> datos = new ArrayList();
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando consultar Solicitud Tarjeta...");
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select cedula,nombre1,nombre2,apellido1,apellido2,"
            + "id_producto,nombre,id_solicitud" +
            ",estado,cliente_cedula,producto_id_producto " +
            "from cliente cl,producto pr,solicitud sl " +
            "where cl.cedula = '"+ p.getCedula()+"' AND " +
            "sl.cliente_cedula = cl.cedula AND " +
            "pr.id_producto = sl.producto_id_producto ");
            while (rs.next())
            { 
                Reportes rep = new Reportes();
                rep.setCedula(rs.getString(1));
                rep.setNombre1(rs.getString(2));
                rep.setNombre2(rs.getString(3));
                rep.setApellido1(rs.getString(4));
                rep.setApellido2(rs.getString(5));
                rep.setId_producto(rs.getString(6));
                rep.setNombre(rs.getString(7));
                rep.setId_solicitud(rs.getString(8));
                rep.setEstado(rs.getString(9));
                rep.setCliente_cedula(rs.getString(10));
                rep.setProducto_id_producto(rs.getString(11));
                datos.add(rep);
                
            }
            
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando Solicitud Tarjeta fin..." + datos.size());   
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReportesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
}
